<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>

<script src="js/form_validasi_kegiatan.js"></script>

<form class="form-horizontal" method="POST" action="proses_kegiatan.php">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Kode</label>  
  <div class="col-md-4">
  <input id="kode" name="kode" type="text" placeholder="Masukkan Kode" class="form-control input-md" value="<?php echo $data['kode']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Judul</label>  
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="Masukkan Judul" class="form-control input-md" value="<?php echo $data['judul']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="narasumber">Narasumber</label>  
  <div class="col-md-4">
  <input id="narasumber" name="narasumber" type="text" placeholder="Masukkan Narasumber" class="form-control input-md" value="<?php echo $data['narasumber']?>">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="deskripsi">Deskripsi</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="deskripsi" name="deskripsi"><?php echo $data['deskripsi']?></textarea>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori">Kategori</label>
  <div class="col-md-4">
    <select id="kategori" name="kategori" class="form-control">
      <option value="0">Pilih Kategori</option>
      <option value="1">Seminar</option>
      <option value="2">Workshop</option>
      <option value="3">Training</option>
      <option value="4">Conferences</option>
      <option value="5">Kuliah Umum</option>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>